const baseUrl = "https://forms-47.herokuapp.com/";
const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjllZGI4YmI5YWJiYmQwYTVjMDMwMTYiLCJuYW1lIjoidmlrYXMgdGl3YXJpIiwiZW1haWwiOiJ2aWthc3Rpd2FyaTcwODQwOUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYSQxMiR1L3ZDOER6NW5qUTZ4YjhHRlNXQ1EuMUt3bzJWSlg4aUEvQmhsY0g5MnZ2SlpPZ2E4T2p2RyIsInJvbGUiOiI2Mjk5YjRmZmUzZDIwMDRjMGE1NDVjMzIiLCJpYXQiOjE2NTQ4MzM0MDgsImV4cCI6MTY1NDkxOTgwOH0.TVcBFJ44ypTO9clWexFJAAHZLlcsZfQ28Ha2Mxdt6_4";
const postFormDetails = async (question: questionFormat) => {
  const postResponse = await fetch(`${baseUrl}question`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify(question),
  });
  return await postResponse.json();
};

const getFormDetails = async () => {
  const getResponse = await fetch(`${baseUrl}question`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  return await getResponse.json();
};

const deleteQuestion = async (position:number) => {
  try {
      const response = await fetch(`https://forms-47.herokuapp.com/question/${position}`, {
          method: 'DELETE',
          headers: {
            "Content-Type": "application/json",
            Authorization: token,
          },
        });
        return await response.json();
  } catch (e) {
      console.log('SOMETHING WENT WRONG');
  }
}


const patchQuestion = async(id:string, shift:string) =>{
  try {
    const response = await fetch(`${baseUrl}question/shift/${id}`, {
        method: 'PATCH',
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify({'shiftAction': shift}),
      });
      return await response.json();
} catch (e) {
    console.log('SOMETHING WENT WRONG');
}

}


class questionFormatClass implements questionFormat {
  text: string = "";
  type: string = "";
  options!: string[];
}
interface questionFormat {
  text: string;
  type: string;
  options?: string[];
}
