"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const baseUrl = "https://forms-47.herokuapp.com/";
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjllZGI4YmI5YWJiYmQwYTVjMDMwMTYiLCJuYW1lIjoidmlrYXMgdGl3YXJpIiwiZW1haWwiOiJ2aWthc3Rpd2FyaTcwODQwOUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYSQxMiR1L3ZDOER6NW5qUTZ4YjhHRlNXQ1EuMUt3bzJWSlg4aUEvQmhsY0g5MnZ2SlpPZ2E4T2p2RyIsInJvbGUiOiI2Mjk5YjRmZmUzZDIwMDRjMGE1NDVjMzIiLCJpYXQiOjE2NTQ4MzM0MDgsImV4cCI6MTY1NDkxOTgwOH0.TVcBFJ44ypTO9clWexFJAAHZLlcsZfQ28Ha2Mxdt6_4";
const postFormDetails = (question) => __awaiter(void 0, void 0, void 0, function* () {
    const postResponse = yield fetch(`${baseUrl}question`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        },
        body: JSON.stringify(question),
    });
    return yield postResponse.json();
});
const getFormDetails = () => __awaiter(void 0, void 0, void 0, function* () {
    const getResponse = yield fetch(`${baseUrl}question`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        },
    });
    return yield getResponse.json();
});
const deleteQuestion = (position) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield fetch(`https://forms-47.herokuapp.com/question/${position}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
                Authorization: token,
            },
        });
        return yield response.json();
    }
    catch (e) {
        console.log('SOMETHING WENT WRONG');
    }
});
const patchQuestion = (id, shift) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield fetch(`${baseUrl}question/shift/${id}`, {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                Authorization: token,
            },
            body: JSON.stringify({ 'shiftAction': shift }),
        });
        return yield response.json();
    }
    catch (e) {
        console.log('SOMETHING WENT WRONG');
    }
});
class questionFormatClass {
    constructor() {
        this.text = "";
        this.type = "";
    }
}
