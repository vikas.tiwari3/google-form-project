"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let typeOfQuestion;
const handleQuestionType = (event) => {
    const eventTarget = event.target;
    eventTarget.getAttribute("data-option") === "true" ? addDelEl.classList.add("show") : addDelEl.classList.add("hide");
    typeOfQuestion = `${eventTarget.getAttribute("data-type")}`;
    optionBox.innerHTML = "";
    const input = document.createElement("input");
    optionBox.appendChild(input);
    input.classList.add("option-input");
};
const addInput = () => {
    const input = document.createElement("input");
    optionBox.appendChild(input);
    input.classList.add("option-input");
};
const dltInput = () => {
    optionBox.removeChild(optionBox.lastChild);
};
const getForm = () => __awaiter(void 0, void 0, void 0, function* () {
    displayArea.innerHTML = "";
    const resp = yield getFormDetails();
    const { data } = resp;
    const { questions } = data;
    const optionDisplay = questions.option;
    for (let index of questions) {
        const displayContainer = document.createElement("div");
        const qstnType = document.createElement("div");
        const typeSpan = document.createElement("span");
        const mainDiv = document.createElement("div");
        const question = document.createElement("h3");
        const parentDiv = document.createElement("div");
        const childDiv = document.createElement("div");
        const upSpan = document.createElement("span");
        const downSpan = document.createElement("span");
        const deleteBtn = document.createElement("div");
        const deleteSpan = document.createElement("span");
        const optionValue = document.createElement("div");
        if (index.options) {
            for (let option of index.options) {
                const optionSpan = document.createElement("span");
                optionSpan.innerText = option;
                optionValue.appendChild(optionSpan);
            }
        }
        switch (index.type) {
            case "6299b4ffe3d2004c0a545c36":
                typeSpan.innerText = "DropDown";
                break;
            case "6299b4ffe3d2004c0a545c37":
                typeSpan.innerText = "Checkbox";
                break;
            case "6299b4ffe3d2004c0a545c39":
                typeSpan.innerText = "Numeric";
                break;
            case "6299b4ffe3d2004c0a545c38":
                typeSpan.innerText = "Text";
                break;
        }
        question.innerHTML = index.text;
        questionInput.value = "";
        upSpan.innerText = "^";
        downSpan.innerText = "v";
        deleteSpan.innerText = "X";
        deleteBtn.appendChild(deleteSpan);
        childDiv.appendChild(upSpan);
        childDiv.appendChild(downSpan);
        parentDiv.appendChild(childDiv);
        parentDiv.appendChild(deleteBtn);
        qstnType.appendChild(typeSpan);
        mainDiv.appendChild(question);
        mainDiv.appendChild(optionValue);
        displayContainer.appendChild(mainDiv);
        displayContainer.appendChild(qstnType);
        displayContainer.appendChild(parentDiv);
        displayArea.appendChild(displayContainer);
        question.classList.add("question-heading");
        childDiv.classList.add("toggles");
        parentDiv.classList.add("toggle-btn");
        mainDiv.classList.add("display-area");
        deleteBtn.classList.add("delete-btn");
        deleteSpan.classList.add("delete-btn-span");
        upSpan.classList.add("up-span");
        downSpan.classList.add("down-span");
        optionValue.classList.add("option-value");
        displayContainer.classList.add("display-container");
        qstnType.classList.add("qstn-type");
        deleteBtn.addEventListener("click", () => __awaiter(void 0, void 0, void 0, function* () {
            yield deleteQuestion(index.position);
            getForm();
        }));
        upSpan === null || upSpan === void 0 ? void 0 : upSpan.addEventListener("click", () => __awaiter(void 0, void 0, void 0, function* () {
            yield patchQuestion(index._id, "UP");
            getForm();
        }));
        downSpan === null || downSpan === void 0 ? void 0 : downSpan.addEventListener("click", () => __awaiter(void 0, void 0, void 0, function* () {
            yield patchQuestion(index._id, "DOWN");
            getForm();
        }));
    }
    console.log(questions);
    return {};
});
const submitForm = () => {
    const OptionData = document.querySelectorAll(".option-input");
    let optionArray = [];
    OptionData.forEach((option) => {
        optionArray.push(option.value + "");
    });
    saveQuestionData({
        text: `${questionInput.value}`,
        type: `${typeOfQuestion}`,
        options: optionArray,
    });
};
const saveQuestionData = (questionData) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { message: string } = yield postFormDetails(questionData);
        alert("done");
    }
    catch (e) {
        alert("something went wrong");
    }
    getForm();
    return {};
});
getForm();
